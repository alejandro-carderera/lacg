# README #

### Outline ###

Contains the data necessary to replicate the video-colocalization instance from: Joulin, Armand, Kevin Tang, and Li Fei-Fei. "Efficient image and video co-localization with frank-wolfe algorithm." European Conference on Computer Vision. Springer, Cham, 2014. This example was also used in: Lacoste-Julien, Simon, and Martin Jaggi. "On the global linear convergence of Frank-Wolfe optimization variants." Advances in Neural Information Processing Systems. 2015.

The specific dataset used is the aeroplane dataset from the Youtube-Objects dataset in Prest, Alessandro, et al. "Learning object class detectors from weakly annotated video." 2012 IEEE Conference on Computer Vision and Pattern Recognition. IEEE, 2012.

### Files ###

* aeroplane_data_small:
Contains the Hessian of the objective function used in the minimization problem, for a total of 660 variables. Also contains the flow polytope that defines the feasible region, and which will allow us to define the linear optimization oracle.